package ru.andreymarkelov.atlas.plugins.spacementions.macro;

import java.util.Map;
import java.util.Set;

import org.apache.velocity.VelocityContext;

import ru.andreymarkelov.atlas.plugins.spacementions.util.PermissionEvaluator;
import ru.andreymarkelov.atlas.plugins.spacementions.util.UserRenderer;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.security.ContentPermission;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;

public class ContentReadersMentionsMacro implements Macro {
    private static final String TEMPLATE = "/ru/andreymarkelov/atlas/plugins/spacementions/templates/pagereaders-body.vm";

    private final SettingsManager settingsManager;
    private final PageManager pageManager;
    private final PermissionEvaluator permissionEvaluator;

    public ContentReadersMentionsMacro(
            SettingsManager settingsManager,
            PageManager pageManager,
            PermissionEvaluator permissionEvaluator) {
        this.pageManager = pageManager;
        this.permissionEvaluator = permissionEvaluator;
        this.settingsManager = settingsManager;
    }

    @Override
    public String execute(
            Map<String, String> parameters,
            String body,
            ConversionContext context) throws MacroExecutionException {
        Set<String> users = permissionEvaluator.getSpaceUsersForView(context.getSpaceKey());

        ContentTypeEnum type = context.getEntity().getTypeEnum();
        if (type == ContentTypeEnum.PAGE) {
            Page page = pageManager.getPage(context.getSpaceKey(), context.getPageContext().getPageTitle());
            if (page != null) {
                Set<String> restrictedUsers = permissionEvaluator.getPageRestrictedUsers(ContentPermission.VIEW_PERMISSION, page);
                if (restrictedUsers != null && !restrictedUsers.isEmpty()) {
                    users.retainAll(restrictedUsers);
                }
            }
        } else if (type == ContentTypeEnum.COMMENT) {
            Page page = pageManager.getPage(context.getSpaceKey(), context.getPageContext().getPageTitle());
            if (page != null) {
                Set<String> restrictedUsers = permissionEvaluator.getPageRestrictedUsers(ContentPermission.VIEW_PERMISSION, page);
                if (restrictedUsers != null && !restrictedUsers.isEmpty()) {
                    users.retainAll(restrictedUsers);
                }
            }
        }

        VelocityContext contextMap = new VelocityContext(MacroUtils.defaultVelocityContext());
        contextMap.put("users", permissionEvaluator.convertUsernamesToObjects(users));
        contextMap.put("renderer", new UserRenderer(settingsManager.getGlobalSettings().getBaseUrl()));
        return VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }
}
