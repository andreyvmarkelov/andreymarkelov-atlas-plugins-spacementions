package ru.andreymarkelov.atlas.plugins.spacementions.util;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.core.ContentPermissionManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.ContentPermissionSet;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.EntityException;
import com.atlassian.user.GroupManager;

public class PermissionEvaluator {
    private Logger log = LoggerFactory.getLogger(PermissionEvaluator.class);

    private final UserAccessor userAccessor;
    private final GroupManager groupManager;
    private final SpaceManager spaceManager;
    private final SpacePermissionManager spacePermissionManager;
    private final ContentPermissionManager contentPermissionManager;

    private PermissionEvaluator(
            UserAccessor userAccessor,
            GroupManager groupManager,
            SpaceManager spaceManager,
            SpacePermissionManager spacePermissionManager,
            ContentPermissionManager contentPermissionManager) {
        this.userAccessor = userAccessor;
        this.groupManager = groupManager;
        this.spaceManager = spaceManager;
        this.spacePermissionManager = spacePermissionManager;
        this.contentPermissionManager = contentPermissionManager;
    }

    public Set<ConfluenceUser> convertUsernamesToObjects(Set<String> usernames) {
        Set<ConfluenceUser> userObjs = new LinkedHashSet<ConfluenceUser>();
        for (String username : usernames) {
            ConfluenceUser userObj = userAccessor.getUserByName(username);
            if (userObj != null) {
                userObjs.add(userObj);
            }
        }
        return userObjs;
    }

    private Set<String> getGroupUsers(String group) {
        Set<String> groupUsers = new TreeSet<String>();
        try {
            groupUsers.addAll(userAccessor.getMemberNamesAsList(groupManager.getGroup(group)));
        } catch (EntityException e) {
            log.warn("PermissionEvaluator::getGroupUsers - Error reading group", e);
        }
        return groupUsers;
    }

    public Set<String> getPageRestrictedUsers(
            String permissionType,
            Page page) {
        Set<String> restrictedUsers = new TreeSet<String>();
        Set<String> parentRestrictedUsers = new TreeSet<String>();
        if (page.getParent() != null) {
            parentRestrictedUsers = getPageRestrictedUsers(permissionType, page.getParent());
        }

        List<ContentPermissionSet> permissionSets = contentPermissionManager.getContentPermissionSets(page.getEntity(), permissionType);
        for (ContentPermissionSet permissionSet : permissionSets) {
            List<String> groups = permissionSet.getGroupNames();
            for (String group : groups) {
                restrictedUsers.addAll(getGroupUsers(group));
            }
            List<String> users = permissionSet.getUserNames();
            for (String user : users) {
                restrictedUsers.add(user);
            }
        }

        if (page.getParent() != null) {
            if (!parentRestrictedUsers.isEmpty()) {
                if (!restrictedUsers.isEmpty()) {
                    parentRestrictedUsers.retainAll(restrictedUsers);
                }
            } else {
                parentRestrictedUsers.addAll(restrictedUsers);
            }
        } else {
            return restrictedUsers;
        }

        return parentRestrictedUsers;
    }

    @SuppressWarnings("unchecked")
    public Set<String> getSpaceUsersForEdit(String spaceKey, String spacePermission) {
        Set<String> spaceUsers = new TreeSet<String>();

        Space space = spaceManager.getSpace(spaceKey);
        if (space == null) {
            return spaceUsers;
        }

        Map<String, Long> groups = spacePermissionManager.getGroupsForPermissionType(spacePermission, space);
        for (String group : groups.keySet()) {
            spaceUsers.addAll(getGroupUsers(group));
        }
        Map<String, Long> users = spacePermissionManager.getUsersForPermissionType(spacePermission, space);
        for (String user : users.keySet()) {
            spaceUsers.add(user);
        }

        return spaceUsers;
    }

    @SuppressWarnings("unchecked")
    public Set<String> getSpaceUsersForView(String spaceKey) {
        Set<String> spaceUsers = new TreeSet<String>();

        Space space = spaceManager.getSpace(spaceKey);
        if (space == null) {
            return spaceUsers;
        }

        Map<String, Long> groups = spacePermissionManager.getGroupsForPermissionType(SpacePermission.VIEWSPACE_PERMISSION, space);
        for (String group : groups.keySet()) {
            spaceUsers.addAll(getGroupUsers(group));
        }
        Map<String, Long> users = spacePermissionManager.getUsersForPermissionType(SpacePermission.VIEWSPACE_PERMISSION, space);
        for (String user : users.keySet()) {
            spaceUsers.add(user);
        }

        return spaceUsers;
    }
}
