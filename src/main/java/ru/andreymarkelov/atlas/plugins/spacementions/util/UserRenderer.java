package ru.andreymarkelov.atlas.plugins.spacementions.util;

import com.atlassian.confluence.user.ConfluenceUser;

public class UserRenderer {
    private String baseUrl;

    public UserRenderer(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String renderUser(ConfluenceUser user) {
        StringBuilder sb = new StringBuilder();
        sb.append("<a data-base-url='").append(baseUrl).append("'")
                .append(" data-linked-resource-id='").append(user.getKey().getStringValue()).append("'")
                .append(" data-linked-resource-type='userinfo'")
                .append(" title=''")
                .append(" class='confluence-userlink user-mention userlink-0'")
                .append(" data-username='").append(user.getName()).append("'")
                .append(" data-linked-resource-default-alias='").append(user.getName()).append("'")
                .append(" href='").append(baseUrl).append("display/~").append(user.getName()).append("'")
                .append(" data-user-hover-bound='true'")
                .append(">").append(user.getFullName()).append("</a>");
        return sb.toString();
    }
}
